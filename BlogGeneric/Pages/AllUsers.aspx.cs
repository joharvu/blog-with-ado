﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.Repos;

namespace BlogGeneric.Pages
{
    public partial class AllUsers : System.Web.UI.Page
    {
        UserRepo user = new UserRepo();
        private static string userid;
        protected void Page_Load(object sender, EventArgs e)
        {
            editUserForm.Visible = false;

        }

        protected void UserRepeater_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "DeleteUser")
            {

                HiddenField userId = userHfId;
                string UId = userId.ToString();
                Label userID = e.Item.FindControl("UserId") as Label;
                UId = userID.Text;
                userid = UId;
                int id = Convert.ToInt32(userid);
                user.DeleteUser(id);
                UserRepeater.DataBind();

            }


            if (e.CommandName == "EditUser")
            {
                editUserForm.Visible = true;

                HiddenField userId = userHfId;
                string UId = userId.ToString();
                Label userID = e.Item.FindControl("UserId") as Label;
                UId = userID.Text;
                userid = UId;
                Label username = e.Item.FindControl("UserName") as Label;
                Label useremail = e.Item.FindControl("UserEmail") as Label;
                Label userdob = e.Item.FindControl("UserDob") as Label;
                EditUserName.Value = username.Text;
                EditUserEmail.Value = useremail.Text;
                EditUserDob.Value = userdob.Text;

            }
        }

        protected void btnUpdateUser_OnClick(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(userid);
            string userName = EditUserName.Value;
            string userEmail = EditUserEmail.Value;
            string userDob = EditUserDob.Value;

            user.UpdateUser(id, userName, userEmail, userDob);
            UserRepeater.DataBind();

        }

        protected void btnUpdateCancel_OnServerClick(object sender, EventArgs e)
        {
            editUserForm.Visible = false;
        }


    }
}