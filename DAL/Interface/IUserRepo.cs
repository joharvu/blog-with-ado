﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface
{
    public interface IUserRepo
    {
        void CreateUser(string userName, string userEmail, string userDob);
        
        void UpdateUser(int Id, string userName, string userEmail, string userDob);

        void DeleteUser(int id);

        Dictionary<string, object> UserNames();

        //public abstract void FindUser();
    }
}
