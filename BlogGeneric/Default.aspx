﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BlogGeneric._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    
    <div class="main-page">
        <div class="container text-center">
            <div class="row">
                <div class="home-btns">
                    <a href="Pages/CreateUser.aspx">Create User</a>
                </div>
                <div class="home-btns">
                    <a href="Pages/AllUsers.aspx">All Users</a>
                </div>
                <div class="home-btns">
                    <a href="Pages/CreatePost.aspx">Create Post</a>
                </div>
                <div class="home-btns">
                    <a href="Pages/AllPosts.aspx">All Posts</a>
                </div>
            </div>
        </div>
    </div>

    
</asp:Content>
