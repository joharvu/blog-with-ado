﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.Repos;

namespace BlogGeneric.Pages
{
    public partial class CreatePost : System.Web.UI.Page
    {
        PostRepo post = new PostRepo();
        UserRepo user = new UserRepo();

        protected void Page_Load(object sender, EventArgs e)
        {

            Dictionary<string, object> usersList = user.UserNames();

            foreach (var USERS in usersList)
            {
                UsersList.Items.Add(new ListItem( USERS.Value.ToString() , USERS.Key));
            }

            
        }

        protected void btnCreatePost_OnClick(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(UsersList.Value);
            string postTitle = PostTitle.Value;
            string postBody = PostBody.Value;
            string postDate = PostDate.Value;
            
            post.CreatePost(userId, postTitle, postBody, postDate);
        }

        
    }
}