﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreatePost.aspx.cs" Inherits="BlogGeneric.Pages.CreatePost" %>
<asp:Content ID="CreatePost" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div class="container">
        <div class="row">
            <div id="create-post-form">
                <asp:HiddenField runat="server" ID="hfID"/>
                <div class="form-group">
                    <label>Select User:</label>
                    <select ID="UsersList" runat="server">

                    </select>
                    
                    
                </div>
                <div class="form-group">
                    <label>Post Title:</label>
                    <input type="text" class="form-control" ID="PostTitle" runat="server" />
                </div>
                <div class="form-group">
                    <label>Post Body:</label>
                    <textarea type="text" class="form-control" rows="6" ID="PostBody" runat="server"></textarea>
                </div>
                <div class="form-group">
                    <label>Post Date:</label>
                    <input type="text" class="form-control" ID="PostDate" runat="server"/>
                </div>
                <asp:LinkButton ID="btnCreatePost" OnClick="btnCreatePost_OnClick" Text="Create Post" CssClass="btn btn-default" runat="server"></asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
