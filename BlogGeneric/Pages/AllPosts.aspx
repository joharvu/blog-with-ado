﻿<%@ Page Title="All Posts" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllPosts.aspx.cs" Inherits="BlogGeneric.Pages.AllPosts" %>
<asp:Content ID="AllPosts" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div id="editPostForm" Visible="False" runat="server">
        <div class="container">
            <div class="row">
                <div id="create-user-form">
                    <asp:HiddenField ID="postHfId" runat="server"/>
                    <div class="form-group">
                        <label>Post Title:</label>
                        <input type="text" class="form-control" id="EditUserName" runat="server" />
                    </div>
                    <div class="form-group">
                        <label>Post Body:</label>
                        <textarea type="text" class="form-control" rows="6" id="EditUserEmail" runat="server"></textarea>
                    </div>
                    <asp:LinkButton ID="btnUpdatePost" Text="Update Post" CssClass="btn btn-default" OnClick="btnUpdatePost_OnClick" runat="server"></asp:LinkButton>
                    <input type="button" id="btnUpdateCancel" value="Cancel" class="btn btn-default" OnServerClick="btnUpdateCancel_OnServerClick" runat="server"/>

                </div>
            </div>
        </div>
    </div>


    
    <table id="all-posts">
        <thead>
        <tr>
            <th>Post Id</th>
            <th>User Name</th>
            <th>Post Title</th>
            <th></th>
            <th>Total Date</th>
            <th>Total Likes</th>
            <th>Total Comments</th>
            <th>Action</th>
            <th>Action</th>
        </tr>
        </thead>
    

        <tbody>
        
            

    <asp:Repeater ID="AllPostsRepeater" runat="server" OnItemCommand="AllPostsRepeater_OnItemCommand" DataSourceID="SqlDataSourcePostswithUser">
        
        <ItemTemplate>
            <tr>
                
                <td> <asp:Label Id="PostId" runat="server" Text='<%# Eval("PostID") %>'></asp:label> </td>
                <td> <asp:Label Id="UserName" runat="server" Text='<%# Eval("UserName") %>'></asp:label> </td>
                <td> <asp:Label Id="PostTitle" runat="server" Text='<%# Eval("PostTitle") %>'></asp:label> </td>
                <td> <asp:Label Id="PostBody" runat="server" Visible="False" Text='<%# Eval("PostBody") %>'></asp:label> </td>
                <td> <asp:Label Id="PostDate" runat="server" Text='<%# Eval("PostDate","{0:d}") %>'></asp:label> </td>
                <td> <asp:Label Id="TotalLikes" runat="server" Text='<%# Eval("TotalLikes") %>'></asp:label> </td>
                <td> <asp:Label Id="TotalComments" runat="server" Text='<%# Eval("TotalComments") %>'></asp:label> </td>

                
                <td> 
                    <asp:LinkButton ID="editBtnPost" CssClass="btn btn-default edit-button" Text="Edit" CommandName="EditPost" runat="server"></asp:LinkButton>
                </td>
                <td> 
                    <asp:LinkButton ID="delBtnPost" CssClass="btn btn-default del-button" Text="Delete" CommandName="DeletePost" runat="server"></asp:LinkButton>
                </td>
            </tr>
        </ItemTemplate>

    </asp:Repeater>
        </tbody>

    </table>

    <asp:SqlDataSource ID="SqlDataSourcePostswithUser" runat="server" ConnectionString="<%$ ConnectionStrings:DbBlogConnectionString %>" SelectCommand="SELECT Posts.ID AS PostID, Posts.UserID, Posts.PostTitle, Posts.PostBody, Posts.PostDate, Posts.TotalComments, Posts.TotalLikes, Users.UserName FROM Posts INNER JOIN Users ON Posts.UserID = Users.ID"></asp:SqlDataSource>


</asp:Content>
