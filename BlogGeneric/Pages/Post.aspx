﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Post.aspx.cs" Inherits="BlogGeneric.Pages.Post" %>
<asp:Content ID="Post" ContentPlaceHolderID="MainContent" runat="server">
    
    
    
    <div id="editCommentForm" Visible="False" runat="server">
        <div class="container">
            <div class="row">
                <div id="create-user-form">
                    <asp:HiddenField ID="cmntId" runat="server"/>
                    <div class="form-group">
                        <label>Comment Body:</label>
                        <input type="text" class="form-control" id="EditCommentBody" runat="server"/>
                    </div>
                    <asp:LinkButton ID="btnUpdateComment" Text="Update Comment" CssClass="btn btn-default" OnClick="btnUpdateComment_OnClick" runat="server"></asp:LinkButton>
                    <input type="button" id="btnUpdateCancel" value="Cancel" class="btn btn-default" OnServerClick="btnUpdateCancel_OnServerClick" runat="server"/>

                </div>
            </div>
        </div>
    </div>
    
    
    

    <div class="container">
        <div class="row text-center">
            <div class="post-details">
                <div class="post-title">
                    <%= pTitle %>
                </div>                
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-center">
                <div class="post-author">
                    By <%= pAuthor %>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="post-author">
                    Date: <%= pDate %>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="post-body">
                <%= pBody %>
            </div>
        </div>
        
        
        <%--Comment box for Rehan--%>
        
        <hr>
        <div class="col-md-12">
            <div class="likeSection">
               
             <asp:LinkButton ID="LikeBtn" CssClass="btn btn-primary" OnClick="LikeBtn_Click" runat="server">Like</asp:LinkButton>
              <asp:LinkButton ID="UnlikeBtn" CssClass="btn btn-danger" OnClick="UnlikeBtn_Click"  runat="server">UnLike</asp:LinkButton>
            </div>
        </div>
        
        <!-- Comments Form -->
        <div class="card col-md-8">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
                    <select ID="UsersList" runat="server">

                    </select>
                    <p></p>
                    <div class="form-group">
                        <textarea ID="CommentBody" class="form-control" rows="3" runat="server"></textarea>
                    </div>
                    <asp:LinkButton ID="AddComment" CssClass="btn btn-primary" OnClick="AddComment_OnClick" runat="server">Submit</asp:LinkButton>
                
            </div>
        </div>
        <div class="col-md-4"></div>
        
        <div class="col-md-8 all-comments card">
            <h5 class="comment-header">All Comments:</h5>
         
                <asp:Repeater ID="CommentsRepeater" runat="server" OnItemCommand="CommentsRepeater_OnItemCommand" DataSourceID="SqlDataSourceComments">
                    
                    <ItemTemplate>
                        <div class="row">
                            <asp:HiddenField ID="cmntHfId" Value='<%# Eval("CommentID") %>' runat="server"/>
                            <div class="comment-author">
                                <asp:Label ID="commentAuthor" Text='<%# Eval("UserName") %>' runat="server"></asp:Label>
                            </div>
                            <div class="comment-body">
                                <asp:Label ID="commentBody" Text='<%# Eval("CommentBody") %>' runat="server"></asp:Label>
                            </div>
                            
                            <div class="comment-btns">
                                <asp:LinkButton ID="editBtnComment" CssClass="btn btn-default" Text="Edit" CommandName="EditComment" runat="server"></asp:LinkButton>
                                <asp:LinkButton ID="delBtnComment" CssClass="btn btn-danger" Text="Delete" CommandName="DeleteComment" runat="server"></asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>

                </asp:Repeater>
                <asp:SqlDataSource ID="SqlDataSourceComments" runat="server" ConnectionString="<%$ ConnectionStrings:DbBlogConnectionString %>" SelectCommand="SELECT Comments.ID AS CommentID, Comments.UserID, Comments.PostID, Comments.CommentBody, Users.UserName FROM Comments INNER JOIN Users ON Comments.UserID = Users.ID"></asp:SqlDataSource>
        </div>



    </div>
    

</asp:Content>
