﻿using DAL.Database;
using DAL.Interface;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL.Repos
{
  public class CommentRepo : ICommentRepo
    {
        DbContext<Comment> dbContext = new DbContext<Comment>();

       

        public void CreateComment(int userid, int postid, string commentbody)
        {
            string qry = "insert into Comments(UserID,PostID,CommentBody)values(@UserID,@PostID,@CommentBody)";
            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic.Add("@UserID", userid);
            dic.Add("@PostID", postid);
            dic.Add("@CommentBody", commentbody);

            if (dbContext.OpenDb())
            {
                dbContext.Insert(qry, dic);
                dbContext.CloseDb();
            }
        }

    
        public void DeleteComment(int id)
        {
            string qry = "DELETE FROM Comments WHERE ID=@ID";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@ID", id);
            

            if (dbContext.OpenDb())
            {
                dbContext.Delete(qry, dic);
                dbContext.CloseDb();
            }
        }

 
        public void UpdateComment(string commentbody,int id)
        {
            string qry = "UPDATE Comments SET CommentBody = @CommentBody WHERE ID=@ID";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@CommentBody", commentbody);
            dic.Add("@ID", id);

            if (dbContext.OpenDb())
            {
                dbContext.Update(qry, dic);
                dbContext.CloseDb();
            }
        }
    }
}
