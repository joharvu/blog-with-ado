﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface
{

    public interface ICommentRepo
    {
        void CreateComment(int userid, int postid, string commentbody);

        void UpdateComment(string commentbody, int id);

        void DeleteComment(int id);
    }
  
}
