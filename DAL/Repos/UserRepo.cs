﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Database;
using DAL.Models;
using DAL.Interface;

namespace DAL.Repos
{
    public class UserRepo : IUserRepo
    { 
        private DbContext<User> dbContext = new DbContext<User>();

        public void CreateUser(string userName, string userEmail, string userDob)
        {
            string insertQuery = "insert into Users(UserName,UserEmail,UserDOB)values(@UserName,@UserEmail,@UserDob)";
            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                {"@UserName", userName},
                {"@UserEmail", userEmail},
                {"@UserDob", userDob}
            };

            if (dbContext.OpenDb())
            {
                dbContext.Insert(insertQuery, dic);
                dbContext.CloseDb();
            }
        }

        public void DeleteUser(int id)
        {
            string deleteQuery = "delete from Users where ID = @ID";
            Dictionary<string, object> dic = new Dictionary<string, object> {{"@ID", id}};

            if (dbContext.OpenDb())
            {
                dbContext.Delete(deleteQuery, dic);
                dbContext.CloseDb();
            }
        }
        
        public void UpdateUser(int id, string userName, string userEmail, string userDob)
        {
            string updateQuery = "update Users SET UserName = @UserName, UserEmail = @UserEmail, UserDob = @UserDob where ID = @ID ";

            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                {"@ID", id},
                {"@UserName", userName},
                {"@UserEmail", userEmail},
                {"@UserDob", userDob}
            };

            if (dbContext.OpenDb())
            {
                dbContext.Update(updateQuery, dic);
                dbContext.CloseDb();
            }



            //Checking
            //public void FindUser(int id, string table)
            //{
            //    table = "Users";
            //    id = 2;
            //    _sqlDataReader = dbContext.FindById(id, table);
            //    showUser();
            //}

            //public void showUser()
            //{
            //    while (_sqlDataReader.Read())
            //    {
            //        // show your data here 
            //        // iterate your results here
            //        Console.WriteLine("{0}\t{1}\t{2}\t{3}", _sqlDataReader.GetInt32(0),
            //            sqlDataReader.GetString(1), sqlDataReader.GetString(2), _sqlDataReader.GetString(3));
            //    }
            //}
    }

        public Dictionary<string,object> UserNames()
        {
            string table = "Users";
            Dictionary<string,object> dic = dbContext.GetAllUsersName(table); 
            dbContext.CloseDb();

            return dic;

        }

        
    }
}
