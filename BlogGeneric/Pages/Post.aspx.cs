﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlogGeneric.Pages;
using DAL.Repos;

namespace BlogGeneric.Pages
{


    public partial class Post : System.Web.UI.Page
    {
        private Blog blog = new Blog();
        private UserRepo user = new UserRepo();
        private CommentRepo cmnt = new CommentRepo();
        private LikeRepo like = new LikeRepo();
        private static string id;
        public int pId { get; set; }
        public int uId { get; set; }

        public string pTitle { get; set; }
        public string pAuthor { get; set; }
        public string pDate { get; set; }
        public string pBody { get; set; }
        public string pTotalComments { get; set; }
        

        protected void Page_Load(object sender, EventArgs e)
        {

            


            Dictionary<string, object> usersList = user.UserNames();

            foreach (var USERS in usersList)
            {
                UsersList.Items.Add(new ListItem(USERS.Value.ToString(), USERS.Key));
            }


            
            pId = Convert.ToInt32(Request.QueryString["PostId"]);

            pTitle = Request.QueryString["PostTitle"];
            pDate = Request.QueryString["PostDate"];
            pBody = Request.QueryString["PostBody"];
            pTotalComments = Request.QueryString["PostTotalComments"];
            pAuthor = Request.QueryString["UserName"];

            SqlDataSourceComments.SelectCommand += " WHERE PostID = " + pId;








        }

        protected void CommentsRepeater_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "EditComment")
            {

                editCommentForm.Visible = true;

                HiddenField hfCmntId = e.Item.FindControl("cmntHfId") as HiddenField;

                string cmntIdStr = hfCmntId.Value.ToString();

                cmntId.Value = cmntIdStr;
                id = cmntId.Value;



                Label CommentBody = e.Item.FindControl("CommentBody") as Label;
                EditCommentBody.Value = CommentBody.Text;
                

            }

            if (e.CommandName == "DeleteComment")
            {

                HiddenField hfCmntId = e.Item.FindControl("cmntHfId") as HiddenField;


                cmnt.DeleteComment(Convert.ToInt32(hfCmntId.Value));

                CommentsRepeater.DataBind();


            }


        }


        protected void AddComment_OnClick(object sender, EventArgs e)
        {
            int uId = Convert.ToInt32(UsersList.Value);
            string commentBody = CommentBody.Value;

            cmnt.CreateComment(uId, pId, commentBody);

            CommentsRepeater.DataBind();
        }

        

        protected void btnUpdateComment_OnClick(object sender, EventArgs e)
        {

            int cmtid = Convert.ToInt32(id) ;
            string commentBody = EditCommentBody.Value;
            cmnt.UpdateComment(commentBody,cmtid);
            CommentsRepeater.DataBind();
            editCommentForm.Visible = false;
        }

        protected void btnUpdateCancel_OnServerClick(object sender, EventArgs e)
        {
            editCommentForm.Visible = false;
        }

        protected void LikeBtn_Click(object sender, EventArgs e)
        {
           
        }

        protected void UnlikeBtn_Click(object sender, EventArgs e)
        {
            
        }
    }
}