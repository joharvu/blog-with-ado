﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    class Post
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string PostTitle { get; set; }
        public string PostBody { get; set; }
        public string PostDate { get; set; }
        public int TotalComments { get; set; }
       
    }
}
