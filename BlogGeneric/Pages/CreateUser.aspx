﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="BlogGeneric.Pages.CreateUser" %>
<asp:Content ID="CreateUser" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div id="success-message" class="success-message text-center">
        <p>User Successfully Created.</p>
        <input type="button" class="btn-success" value="Close"/>
    </div>
    
    <div class="container">
        <div class="row">
            <div id="create-user-form">
                <asp:HiddenField runat="server" ID="hfID"/>
                
                <div class="form-group">
                    <label>UserName:</label>
                    <input type="text" class="form-control" id="UserName" runat="server" />
                </div>
                <div class="form-group">
                    <label>UserEmail:</label>
                    <input type="email" class="form-control" id="UserEmail" runat="server" />
                </div>
                <div class="form-group">
                    <label>User Date of Birth:</label>
                    <input type="text" class="form-control" id="UserDob" runat="server" />
                </div>
                <asp:LinkButton ID="btnCreateUser" OnClick="btnCreateUser_OnClick" Text="Create User" CssClass="btn btn-default" runat="server"></asp:LinkButton>
                <br /><label id="lblMess" runat="server"></label>
            </div>
        </div>
    </div>

    
</asp:Content>
