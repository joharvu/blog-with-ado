﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DAL.Database
{

    
     
    public class DbContext<T>
    {
        // Data members
        private SqlConnection _connection;
        private SqlCommand _command;
        private string _serverName;
        private string _dbName;
        private string _connectionString;
        private SqlDataAdapter _sqlDataAdapter;
        private DataTable _dataTable;
        private SqlDataReader _sqlDataReader;
        private Dictionary<string, object> _dic = new Dictionary<string, object>();

        // Properties
        public string Table { get; set; }

        // Constructor
        public DbContext()
        {
            //_serverName = @".\SQLEXPRESS";sss
            //_dbName = "DbBlog";
            //_connectionString = String.Format("Data Source ={0}; Initial Catalog = {1}; Integrated Security = True", _serverName, _dbName);
            _connectionString = ConfigurationManager.ConnectionStrings["DbBlogConnectionString"].ConnectionString;
            _connection = new SqlConnection(_connectionString);
            _command = new SqlCommand();
            SqlConnection.ClearAllPools();
            this.OpenDb();
            
        }

        // Open function for open database connection
        public bool OpenDb()
        {
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
                return false;
            }
            else
            {
                return true;
            }
        }

        // Close function for closing database connection
        public void CloseDb()
        {
            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
        }

        // Insert method for inserting data into the database
        public bool Insert(string qry,Dictionary<string,object> dictionary)
        {
            return ExecuteNonQry(qry,dictionary);
        }
        

        // Update method for Updating data into the database
        public bool Update(string qry, Dictionary<string, object> dictionary)
        {
            return ExecuteNonQry(qry, dictionary);
        }
        // Delete method for deleting data from the database
        public bool Delete(string qry, Dictionary<string, object> dictionary)
        {
            return ExecuteNonQry(qry, dictionary);
        }
        // ExecuteNonQry function for executing the query
        public bool ExecuteNonQry(string qry, Dictionary<string,object> dictionary)
        {
            foreach (var variable in dictionary)
            {
                _command.Parameters.AddWithValue(variable.Key, variable.Value);
            }

            _command.CommandText = qry;
            _command.Connection = _connection;
            int ans =  _command.ExecuteNonQuery();
            if (ans == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // overloaded ExecuteNonQry function for executing the query only for string
        public bool ExecuteNonQry(string qry)
        {
            int ans = _command.ExecuteNonQuery();
            if (ans == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        // SelectFromTable function for selecting the data from database table
        public SqlDataReader SelectFromTable(string query)
        {
            if (_connection.State == ConnectionState.Open)
            {
                _command = new SqlCommand(query, _connection);
                _sqlDataReader = _command.ExecuteReader();

            }
            return _sqlDataReader;

        }
        // Function for getting the names of users from Users table
        public Dictionary<string,object> GetAllUsersName(string table)
        {
            string qry = $"SELECT ID,UserName FROM {table}";
            _sqlDataReader = SelectFromTable(qry);
            
            string id;
            
            while (_sqlDataReader.Read())
            {
                var userId = _sqlDataReader.GetValue(0);
                var userName = _sqlDataReader.GetValue(1);
                id = userId.ToString();
                _dic.Add(id, userName);

            }

            return _dic;
        }


        // Find By Id method 
        //public SqlDataReader FindById(int id, string table)
        //{
        //    this.Table = table;
        //    string qry = $"SELECT * FROM {Table} WHERE id = {id}";
        //    _sqlDataReader = SelectFromTable(qry);
        //    return _sqlDataReader;
        //}

        //// Find by Post Title method
        //public SqlDataReader FindByPostTitle(string title, string table)
        //{
        //    this.Table = table;
        //    string qry = $"SELECT * FROM {Table} WHERE PostTitle = {title}";
        //    _sqlDataReader = SelectFromTable(qry);
        //    return _sqlDataReader;
        //}

    }
}


