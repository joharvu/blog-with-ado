﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="BlogGeneric.Pages.HomePage" %>
<asp:Content ID="HomePage" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div class="main-page">
        <div class="container text-center">
            <div class="row">
                <div class="home-btns">
                    <a href="CreateUser.aspx">Create User</a>
                </div>
                <div class="home-btns">
                    <a href="AllUsers.aspx">All Users</a>
                </div>
                <div class="home-btns">
                    <a href="CreatePost.aspx">Create Post</a>
                </div>
                <div class="home-btns">
                    <a href="AllPosts.aspx">All Posts</a>
                </div>
            </div>
        </div>
    </div>
    

</asp:Content>
