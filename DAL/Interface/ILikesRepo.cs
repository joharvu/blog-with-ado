﻿namespace DAL.Interface
{
    public interface ILikesRepo
    {
        void CreateLikes(int userId,int postId);

        void DeleteLikes(int id);
    }
}