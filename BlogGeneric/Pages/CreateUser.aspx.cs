﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.Repos;

namespace BlogGeneric.Pages
{
    public partial class CreateUser : System.Web.UI.Page
    {
        UserRepo user = new UserRepo();


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void Check()
        {
            if (UserName.Value.Length==0&& UserEmail.Value.Length == 0 && UserDob.Value.Length == 0)
            {
                lblMess.InnerText = "Please fill all the fields";
            }
        }
        private void Clear()
        {
            UserName.Value = UserEmail.Value = UserDob.Value = null;
        }

        protected void btnCreateUser_OnClick(object sender, EventArgs e)
        {
            string userName = UserName.Value;
            string userEmail = UserEmail.Value;
            string userDob = UserDob.Value;

            user.CreateUser(userName, userEmail, userDob);
            Check();
            Clear();
        }

        
    }
}