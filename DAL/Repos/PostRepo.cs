﻿using DAL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Database;
using DAL.Models;

namespace DAL.Repos
{
    public class PostRepo : IPostRepo
    {
        DbContext<Post> dbContext = new DbContext<Post>();

   

        public void CreatePost(int userId, string postTitle, string postBody, string postDate)
        {
            string qry = "insert into Posts(UserID,PostTitle,PostBody,PostDate)values(@UserID,@PostTitle,@PostBody,@PostDate)";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@UserID", userId);
            dic.Add("@PostTitle", postTitle);
            dic.Add("@PostBody", postBody);
            dic.Add("@PostDate", postDate);

            if (dbContext.OpenDb())
            {
                dbContext.Insert(qry, dic);
                dbContext.CloseDb();
            }
        }

        public void DeletePost(int id)
        {
            string qry = "DELETE FROM Posts WHERE ID=@ID";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@ID", id);

            if (dbContext.OpenDb())
            {
                dbContext.Delete(qry, dic);
                dbContext.CloseDb();
            }
        }



        public void UpdatePost(int id, string postTitle, string postBody)
        {
            string qry = "UPDATE Posts SET PostTitle = @PostTitle, PostBody = @PostBody Where ID = @ID";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@PostTitle", postTitle);
            dic.Add("@PostBody", postBody);
            dic.Add("@ID", id);

            if (dbContext.OpenDb())
            {
                dbContext.Update(qry, dic);
                dbContext.CloseDb();
            }
        }
    }
}
