﻿<%@ Page Title="All Users" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllUsers.aspx.cs" Inherits="BlogGeneric.Pages.AllUsers" %>
<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="AllUsers" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div id="editUserForm" Visible="False" runat="server">
        <div class="container">
            <div class="row">
                <div id="create-user-form">
                    <asp:HiddenField ID="userHfId" Value="" runat="server"/>
                    <div class="form-group">
                        <label>UserName:</label>
                        <input type="text" class="form-control" id="EditUserName" runat="server" />
                    </div>
                    <div class="form-group">
                        <label>UserEmail:</label>
                        <input type="email" class="form-control" id="EditUserEmail" runat="server" />
                    </div>
                    <div class="form-group">
                        <label>User Date of Birth:</label>
                        <input type="text" class="form-control" id="EditUserDob" runat="server" />
                    </div>
                    <asp:LinkButton ID="btnUpdateUser" Text="Update User" CssClass="btn btn-default" OnClick="btnUpdateUser_OnClick" runat="server"></asp:LinkButton>
                    <input type="button" ID="btnUpdateCancel" value="Cancel" OnServerClick="btnUpdateCancel_OnServerClick" class="btn btn-default" runat="server"/>

                </div>
            </div>
        </div>
    </div>

    <table id="all-users">
        
        <thead>
            <tr>
                <th>User Id</th>
                <th>User Name</th>
                <th>User Email</th>
                <th>User Dob</th>
                <th>Action</th>
                <th>Action</th>
            </tr>
        </thead>
        
        <tbody>
            <asp:Repeater ID="UserRepeater" runat="server" OnItemCommand="UserRepeater_OnItemCommand" DataSourceID="SqlDataSourceUsers">
        
                
                
                        <ItemTemplate>
                            <tr>
                                <td> <asp:Label Id="UserId" runat="server" Text='<%# Eval("ID") %>'></asp:label> </td>
                                <td> <asp:Label Id="UserName" runat="server" Text='<%# Eval("UserName") %>'></asp:label> </td>
                                <td> <asp:Label Id="UserEmail" runat="server" Text='<%# Eval("UserEmail") %>'></asp:label> </td>
                                <td> <asp:Label Id="UserDob" runat="server" Text='<%# Eval("UserDob") %>'></asp:label> </td>
                                <td> 
                                    <asp:LinkButton ID="editBtn" CssClass="btn btn-default edit-button" Text="Edit" CommandName="EditUser" runat="server"></asp:LinkButton>
                                </td>
                                <td> 
                                    <asp:LinkButton ID="delBtn" CssClass="btn btn-default del-button" Text="Delete" CommandName="DeleteUser" runat="server"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                
        
            </asp:Repeater>
        </tbody>
    </table>


<asp:SqlDataSource ID="SqlDataSourceUsers" runat="server" ConnectionString="<%$ ConnectionStrings:DbBlogConnectionString%>" SelectCommand="SELECT * FROM [Users]"></asp:SqlDataSource>


</asp:Content>
