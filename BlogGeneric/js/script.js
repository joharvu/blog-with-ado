﻿$(document).ready(function() {

    $("#MainContent_UserDob").datepicker({
        dateFormat: "yy-mm-dd"
    });

    $("#MainContent_PostDate").datepicker({
        dateFormat: "yy-mm-dd",
        defaultDate: 0
        
    });
    $("#MainContent_PostDate").datepicker(
        "setDate", new Date()
    );

    $("#all-users").DataTable();
    $("#all-posts").DataTable();


    $("#MainContent_UsersList").select2();
    var select2User = $("#MainContent_UsersList")[0].nextSibling;
    select2User.style = "width: 100%;";

    





    $(".edit-button").click(function () {
        
        showEditForm();
        return false;
    });

    $("#btnUpdateCancel").click(function() {
        hideEditForm();
    });



    function showEditForm() {
        
        $("#MainContent_editUserForm").show();
        $("#edit-post-form").show();
        $("#MainContent_editCommentForm").show();
        return false;
        

    }

    function hideEditForm() {
        $("#MainContent_editUserForm").hide();
        $("#edit-post-form").hide();
        $("#MainContent_editCommentForm").hide();
    }

    function showSuccessMsg() {
        $("#success-message").show();
    }

});