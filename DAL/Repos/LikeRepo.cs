﻿using DAL.Interface;
using DAL.Database;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repos
{
    public class LikeRepo : ILikesRepo
    {
        DbContext<Like> dbContext = new DbContext<Like>();
        public void CreateLikes(int userId, int postId)
        {
            string qry = "insert into Likes(UserID,PostID)values(@UserID,@PostID)";
            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic.Add("@UserID", userId);
            dic.Add("@PostID", postId);

            if (dbContext.OpenDb())
            {
                dbContext.Insert(qry, dic);
                dbContext.CloseDb();
            }
        }

        public void DeleteLikes(int id)
        {
            string qry = "DELETE FROM Likes WHERE ID = @ID";
            
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@ID", id);


            if (dbContext.OpenDb())
            {
                dbContext.Delete(qry, dic);
                dbContext.CloseDb();
            }
        }
    }
}