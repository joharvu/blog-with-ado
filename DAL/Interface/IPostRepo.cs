﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface
{
    public interface IPostRepo
    {
        void CreatePost(int userId, string posttitle, string postbody, string postdate);
        
        void UpdatePost(int id, string posttitle, string postbody);

        void DeletePost(int id);

        //public abstract void FindPost();
        ////find post by post title
    }
}
