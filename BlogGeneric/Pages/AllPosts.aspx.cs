﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DAL.Repos;

namespace BlogGeneric.Pages
{
    public partial class AllPosts : System.Web.UI.Page
    {
        PostRepo _allPost= new PostRepo();
        private static string id;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AllPostsRepeater_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "DeletePost")
            {
                HiddenField hfPostId = postHfId;
                string postId = hfPostId.ToString();
                Label lblPostID = e.Item.FindControl("PostId") as Label;
                postId = lblPostID.Text;
                id = postId;
                int pId = Convert.ToInt32(id);
                _allPost.DeletePost(pId);
                AllPostsRepeater.DataBind();
            }

            

            if (e.CommandName == "EditPost")
            {
                
                editPostForm.Visible = true;

                HiddenField hfPostId = postHfId;
                string postId = hfPostId.ToString();
                Label lblPostID = e.Item.FindControl("PostId") as Label;
                postId = lblPostID.Text;
                id = postId;
                Label posttitle = e.Item.FindControl("PostTitle") as Label;
                Label postBody = e.Item.FindControl("PostBody") as Label;
                EditUserName.Value = posttitle.Text;
                EditUserEmail.Value = postBody.Text;



            }
        }

        protected void btnUpdatePost_OnClick(object sender, EventArgs e)
        {
            int pId = Convert.ToInt32(id);
            string postTitle = EditUserName.Value;
            string postBody = EditUserEmail.Value;

            _allPost.UpdatePost(pId,postTitle,postBody);
            AllPostsRepeater.DataBind();
            editPostForm.Visible = false;
        }

        protected void btnUpdateCancel_OnServerClick(object sender, EventArgs e)
        {
            editPostForm.Visible = false;
        }

        
    }
}