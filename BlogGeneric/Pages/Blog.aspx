﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="BlogGeneric.Pages.Blog" %>
<asp:Content ID="Blog" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div class="blog-contents">
        <div class="container">
            
            

            
            <div ID="DIV" runat="server"></div>
            
            <asp:Repeater ID="RepeaterBlog" runat="server" OnItemCommand="RepeaterBlog_OnItemCommand" DataSourceID="SqlDataSourceBlog">
                
                <ItemTemplate>
                    
                    

                    <div class="row">
                        <div class="blog-post-box">
                            
                            <asp:HiddenField ID="PostID" Value='<%# Eval("ID") %>' runat="server"/>
                            <asp:HiddenField ID="UserID" Value='<%# Eval("UserID") %>' runat="server"/>

                            <div class="blog-post-title">
                                <h3> <asp:Label ID="PostTitle" Text='<%# Eval("PostTitle") %>' runat="server"></asp:Label> </h3>
                            </div>
                            <div class="blog-post-date">
                                <p><asp:Label ID="PostDate" Text='<%# Eval("PostDate", "{0:d}") %>' runat="server"></asp:Label></p>
                            </div>
                            <div class="blog-post-author">
                                <p>By <asp:Label ID="UserName" Text='<%# Eval("UserName") %>' runat="server"></asp:Label></p>
                            </div>
                            <div class="blog-post-summary">
                                <p><asp:Label ID="PostBody" Text='<%# Eval("PostBody") %>' runat="server"></asp:Label></p>
                            </div>
                            <div class="blog-post-details">
                                
                                
                                <div id="allComments" class="all-comments">
                                    <asp:DataList ID="AllCommentsData" runat="server"></asp:DataList>
                                </div>
                                
                                <asp:LinkButton CssClass="btn btn-info" ID="btnReadMore" Text="Read More" CommandName="ReadMore" runat="server"></asp:LinkButton>
                                

                            </div>
                        </div>
                    </div>

                            
             
            </ItemTemplate>


            </asp:Repeater>

            <asp:SqlDataSource ID="SqlDataSourceBlog" runat="server" ConnectionString="<%$ ConnectionStrings:DbBlogConnectionString %>" SelectCommand="SELECT Posts.ID, Posts.UserID, Posts.PostTitle, Posts.PostBody, Posts.PostDate, Posts.TotalComments, Posts.TotalLikes, Users.UserName, Comments.CommentBody FROM Posts LEFT OUTER JOIN Users ON Posts.UserID = Users.ID LEFT OUTER JOIN Comments ON Posts.ID = Comments.PostID AND Users.ID = Comments.UserID"></asp:SqlDataSource>

            
            
        </div>

    </div>
</asp:Content>
