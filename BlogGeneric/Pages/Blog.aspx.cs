﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BlogGeneric.Pages
{
    public partial class Blog : System.Web.UI.Page
    {
        
        public int postId { get; set; }
        public int userId { get; set; }
        public string postTitle { get; set; }
        public string postDate { get; set; }
        public string postBody { get; set; }
        public string postTotalComments { get; set; }
        public string userName { get; set; }
        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        

        protected void RepeaterBlog_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ReadMore")
            {
                HiddenField lblPostId = e.Item.FindControl("PostID") as HiddenField;
                HiddenField lblUserId = e.Item.FindControl("UserID") as HiddenField;
                Label lblPostTitle = e.Item.FindControl("PostTitle") as Label;
                Label lblPostDate = e.Item.FindControl("PostDate") as Label;
                Label lblUserName = e.Item.FindControl("UserName") as Label;
                Label lblPostBody = e.Item.FindControl("PostBody") as Label;
                


                postId = Convert.ToInt32(lblPostId.Value);
                userId = Convert.ToInt32(lblUserId.Value);
                userName = lblUserName.Text;
                postTitle = lblPostTitle.Text;
                postDate = lblPostDate.Text;
                postBody = lblPostBody.Text;
                

                

                Response.Redirect("Post.aspx?PostTitle=" + postTitle+"&PostDate="+ postDate + "&PostBody=" + postBody + "&PostTotalComments=" + postTotalComments + "&UserName=" + userName + "&PostId=" + postId + "&UserId=" + userId);
                

            }
        }


        

    }
}